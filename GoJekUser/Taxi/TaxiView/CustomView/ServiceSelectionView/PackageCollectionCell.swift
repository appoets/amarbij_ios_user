//
//  PackageCollectionCell.swift
//  GoJekUser
//
//  Created by Sudar vizhi on 05/04/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit

class PackageCollectionCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var overView: UIView!
    
    var isCurrentPackage = false {
        didSet {
            titleLabel.textColor = isCurrentPackage ? .white : .darkGray
            subTitleLabel.textColor = isCurrentPackage ? .white : .darkGray
            overView.backgroundColor = isCurrentPackage ? .taxiColor : .veryLightGray
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setFont()
    }
    private func setFont(){
        titleLabel.font = .setCustomFont(name: .bold, size: .x14)
        subTitleLabel.font = .setCustomFont(name: .medium, size: .x12)
        overView.backgroundColor = .veryLightGray
        overView.setCornerRadiuswithValue(value: 5)
        
    }

}
